# Hourly Reminder

Android friendly: based on system Alarm Manager, you will never miss an alarm!

This application allows you to remind you about hour passed. User specified hours will be notified using text2speech technology.

# Manual install

    gradle installDebug

# Translate

If you want to translate 'Hourly Reminder' to your language or just add new speak engine please read this:

  * [HOWTO-Translate.md](/docs/HOWTO-Translate.md)

# Screenshots

![01-Home_Shot](/docs/01-Home_Shot.png)
![04-CustomAlarm_Shot](/docs/04-CustomAlarm_Shot.png)

![02-SetFrequency_Shot](/docs/02-SetFrequency_Shot.png)
![03-AddHourly_Shot](/docs/03-AddHourly_Shot.png)

![05-Options01_Shot](/docs/05-Options01_Shot.png)
![06-Options02_Shot](/docs/06-Options02_Shot.png)

![shot](/docs/shot.png)
